//HEART'S COLOR:
//in HTML:
///////////////////
<div class="heart">
    <a class="a-heart" href="#"><img src="red.png" id="heart" class="red"></a>
</div>

// JS like this:
////////////////////
let heart = document.getElementById('heart');
heart.addEventListener('click',()=>{
    if(heart.classList.contains('red')){
        heart.classList.remove('red');
        heart.classList.add('white');
        heart.src = 'white.png';
    }else{
        heart.classList.remove('white');
        heart.classList.add('red');
        heart.src = 'red.png';
    }
});




//TRASHBIN DELETE:
//The HTML:
////////////////
 <div class='del-trashbin'>
                <i class="......" title='Delete'></i>
              </div> 
			  
In JS: with a popup confirmation box
//////////////////////
var delete_menu_btns = document.querySelectorAll('.del-menu-icon-image');
var del_conf_overlay_box = document.querySelector('.del-conf-overl-outer');
var del_conf_cancel_btn = document.querySelector('.del-conf-cancel-btn');
var del_conf_confirm_btn = document.querySelector('.del-conf-confirm-btn');

var Event = {};

delete_menu_btns.forEach(btn => {
    btn.addEventListener('click', showDeleteConfBox);
});

function showDeleteConfBox(event) {
	  Event.remove = event;
    del_conf_overlay_box.classList.add('show-del-conf-overl-box');
}
  
function removeDeleteconfBox() {
    del_conf_overlay_box.classList.remove('show-del-conf-overl-box');
    Event.remove.target.closest('.res-card-outer').classList.add('hide-res-card-outer');
}
  
function cancelDeleteconfBox() {
    del_conf_overlay_box.classList.remove('show-del-conf-overl-box');
    del_conf_confirm_btn.removeEventListener("click", function() {
    	removeDeleteconfBox();
    });
}

del_conf_confirm_btn.addEventListener("click", removeDeleteconfBox);
del_conf_cancel_btn.addEventListener("click", cancelDeleteconfBox);


//FOR THE PROPAGATED EVEN ON REFRESH:use a file with hook. 
///////////////////

function getStorageValue(key, defaultValue) {

  const saved = localStorage.getItem(key);
  const initial = JSON.parse(saved);
  return initial || defaultValue;
}

export const useLocalStorage = (key, defaultValue) => {
  const [value, setValue] = useState(() => {
    return getStorageValue(key, defaultValue);
  });

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value));
  }, [key, value]);

  return [value, setValue];
};

/Then USE IT:
///////////////////////
const [active, setActive] = useLocalStorage("active", "false")




//Fetch the API: in App.js 
/////////////////////////

    constructor(props) {
        super(props);
   
        this.state = {
            items: [],
            DataisLoaded: false
        };
    }
   
    // THEN EXECUTE
   
    componentDidMount() {
        fetch(
"https://jsonplaceholder.typicode.com/users")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    items: json,
                    DataisLoaded: true
                });
            })
    }
    render() {
        const { DataisLoaded, items } = this.state;
        if (!DataisLoaded) return <div>
            <h1> Pleses wait some time.... </h1> </div> ;
   
        return (
        <div className = "App">
            <h1> Fetch data from an api in react </h1>  {
                items.map((item) => ( 
                <ol key = { item.id } >
                    User_Name: { item.username }, 
                    Full_Name: { item.name }, 
                    User_Email: { item.email } 
                    </ol>
                ))
            }
        </div>
    );
}
}

